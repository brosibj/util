package brosious.util;

import java.util.ArrayList;

/**
 * A simple implementation of 2D <code>ArrayList</code>s. Can be instantiated similarly to a normal
 * <code>ArrayList</code>, but now requires both columns and row count for size or no arguments at all.
 * @author Ben
 *
 * @param <Type>
 */
public class ArrayList2d<Type> {

	ArrayList<ArrayList<Type>> a;
	
	public ArrayList2d(){
		a = new ArrayList<ArrayList<Type>>();
	}
	
	public ArrayList2d(int initialColumns, int initialRows) {
		this.setSize(initialColumns, initialRows);
		a.clone();
	}
	
	/**
	 * Empty initialization results in empty 2D arraylist.
	 * Specifying size will initialize the array with that many columsn and rows.
	 * You may also specify an array to use as the back-end. Must be an <i>ArrayList<ArrayList<Type>></i>
	 * @param initialColumns
	 * @param initialRows
	 * <b>OR</b>
	 * @param arr
	 */
	public ArrayList2d(ArrayList<ArrayList<Type>> arr){
		this.a = arr;
	}
	
	/**
	 * Get number of columns,
	 * @return
	 */
	public int columns(){return a.get(0).size();}
	/**
	 * Get number of rows.
	 * @return
	 */
	public int rows(){return a.size();}
	/**
	 * Get item at specific coordinates.
	 * @param column
	 * @param row
	 * @return Item
	 */
	public Type get(int column, int row){return a.get(row).get(column);}
	/**
	 * Set item at specific coordinates.
	 * @param column
	 * @param row
	 * @param v
	 */
	public void set(int column, int row, Type v){a.get(row).set(column, v);}
	/**
	 * Returns true if ArrayList2d is empty.
	 * @return isEmpty
	 */
	public boolean isEmpty(){return this.rows() < 1 && this.columns() < 1;}
	/**
	 * Returns the <i>ArrayList<ArrayList<Type>></i> for extensibility.
	 * @return
	 */
	public ArrayList<ArrayList<Type>> toArray(){return this.a;}
	/**
	 * Returns an empty <i>ArrayList2d<Type></i> version of object.
	 */
	public ArrayList2d<Type> clone(){return new ArrayList2d<Type>(shellOf(this.a));}
	/**
	 * Converts a <i>ArrayList<ArrayList<Type>></i> to a <i>ArrayList2d<Type></i>.
	 * Must instantiate object first to specify type. (kind of pointless)
	 * @param arr
	 * @return
	 */
	public ArrayList2d<Type> convert(ArrayList<ArrayList<Type>> arr){return new ArrayList2d<Type>(arr.get(0).size(),arr.size());}
	/**
	 * Returns the empty 2D arraylist of a given 2D arraylist
	 * @param arr
	 * @return
	 */
	private ArrayList<ArrayList<Type>> shellOf(ArrayList<ArrayList<Type>> arr){
		if (arr.get(0).size() < 1 || arr.size() < 1) return new ArrayList<ArrayList<Type>>();
		else{
			ArrayList<ArrayList<Type>> na = new ArrayList<ArrayList<Type>>();
			ArrayList<Type> inner = new ArrayList<Type>(arr.get(0).size());
			for(int i=0;i<arr.size();i++){na.add(inner);}
			return na;
		}
	}
	
	/**
	 * Set the size of the ArrayList2d.
	 * @param columns
	 * @param rows
	 */
	public void setSize(int columns, int rows){
		if (columns < 1 || rows < 1) a = new ArrayList<ArrayList<Type>>();
		else {
			ArrayList<ArrayList<Type>> na = new ArrayList<ArrayList<Type>>();
			ArrayList<Type> inner = new ArrayList<Type>(columns);
			for(int i=0;i<rows;i++){na.add(inner);}
			
			for(int i=0; i<rows; i++){
				for(int j=0; j<columns; j++){
					na.get(i).set(j, this.get(j, i));
				}
			}
		}
	}
	/**
	 * Tests if given object exists in ArrayList2d
	 * @param t
	 * @return bool
	 */
	public boolean contains(Type t){
		for (int i=0; i<this.rows(); i++){
			for (int j=0; j<this.columns(); j++){
				if (this.a.get(i).get(j) == t) return true;
			}
		}
		return false;
	}


}
