package brosious.util;

/**
 * <code>AnsiColor</code> fully implements ansi color codes for simple color coding console
 * output without hassle. Currently supports all 18 colors in foreground and background, 
 * as well as resetting the style.
 * @author Ben
 *
 */
public class AnsiColor {

	public static final String BLACK = "0;0";
	public static final String RED = "1;0";
	public static final String GREEN = "2;0";
	public static final String BROWN = "3;0";
	public static final String NAVY = "4;0";
	public static final String MAGENTA = "5;0";
	public static final String TEAL = "6;0";
	public static final String GRAY = "7;0";

	public static final String DARKGRAY = "0;1";
	public static final String PINK = "1;1";
	public static final String LIME = "2;1";
	public static final String YELLOW = "3;1";
	public static final String BLUE = "4;1";
	public static final String PURPLE = "5;1";
	public static final String CYAN = "6;1";
	public static final String WHITE = "7;1";

	public static final String RESET = "\u001B[0m";
	
	private static final String START = "\u001B[";
	private static final String ENDCHAR = "m";
	private static final String SEPERATOR = ";";
	private static final String FOREGROUND = "3";
	private static final String BACKGROUND = "4";
	
	private String color;
	
	/**
	 * Provide foreground color and optionally a background color.
	 * @param fc
	 * @param bc
	 */
	public AnsiColor(String fc, String bc) {
		color = START + FOREGROUND + fc + SEPERATOR + BACKGROUND + bc + ENDCHAR;
	}
	public AnsiColor(String fc){
		color = START + FOREGROUND + fc + ENDCHAR;
	}

	/**
	 * Colorize the inputed string, output colorized string.
	 * @param m
	 * @return <i>String</i> colorizedOutput
	 */
	public String colorize(String m){return color+m+RESET;}
	
	/**
	 * Prints string with color. Uses <code>System.out.print()</code>
	 * @param m
	 */
	public void out(String m){System.out.print(color+m+RESET);}
	/**
	 * Prints string with color. Uses <code>System.out.println()</code>
	 * @param m
	 */
	public void outln(String m){System.out.println(color+m+RESET);}
	/**
	 * Prints string with color. Uses <code>System.err.print()</code>
	 * @param m
	 */
	public void err(String m){System.err.print(color+m+RESET);}
	/**
	 * Prints string with color. Uses <code>System.err.println()</code>
	 * @param m
	 */
	public void errln(String m){System.err.println(color+m+RESET);}
	
}
