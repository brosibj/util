package brosious.util;

/**
 * <code>FrameRateHandler</code> is a primative way to handle refreshing the screen 
 * at certain frequencies. Only really useful when used with <code>java.swing</code> 
 * or similar packages.
 * @author Ben
 *
 */
public class FrameRateHandler {

	private int fps;
	private long delay;
	
	/**
	 * Initialize with <i>int</i> fps, <i>long</i> delay (milliseconds between frames),
	 * or nothing for a default of 60 FPS.
	 * @param fps
	 * @param delay
	 */
	public FrameRateHandler(int fps) {
		setFPS(fps);
	}
	
	public FrameRateHandler(long delay) {
		setDelay(delay);
	}
	
	public FrameRateHandler(){setFPS(60);}
	
	/**
	 * Sets FPS and updates delay between frames
	 * @param fps
	 */
	public void setFPS(int fps){
		this.fps = fps;
		this.delay = fps*1000;
	}
	/**
	 * Sets delay between frames and updates FPS
	 * @param delay
	 */
	public void setDelay(long delay){
		this.delay = delay;
		this.fps = (int) delay/1000;
	}
	
	/**
	 * Call this method to pause the correct length.
	 */
	public void pause(){
		try {Thread.sleep(delay);} 
		catch (InterruptedException e) {e.printStackTrace();}
	}
	
	/**
	 * Returns the FPS.
	 * @return fps
	 */
	public int fps(){return this.fps;}
	/**
	 * Returns the delay between frames
	 * @return delay
	 */
	public long delay(){return this.delay;}
	
}
