package brosious.util;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <code>HighQualityRandom</code> uses a unique algorithm to find a better random than that of
 * <code>java.util.Random</code> at the cost of slightly slower speed.<br>
 * The number is determined by two XORShift generators followed by a LCG and multiply
 * with carry generator.
 * @author Ben
 *
 */
public class HighQualityRandom extends Random {
	private Lock l = new ReentrantLock();
	private static final long serialVersionUID = 4101842887655102017L;
	private long v = serialVersionUID;
	private long w = 1;
	private long u;
	private long s;
	
	/**
	 * Create a new HighQualityRandom with a seed or based on the system time at creation.
	 * (System.nanoTime())
	 * @param seed
	 */
	public HighQualityRandom(long seed) {
		l.lock();
		s = seed;
		u = seed ^ v;
	}
	
	public HighQualityRandom() {
		this(System.nanoTime());
	}

	/**
	 * Returns the next random <i>Long</i> in the assigned seed.
	 * Randomness is determined by two XORShift generators combined
	 * with a LCG and a multiply with carry generator.
	 */
	public long nextLong(){
		l.lock();
		try {
		      u = u * 2862933555777941757L + 7046029254386353087L;
		      v ^= v >>> 17;
		      v ^= v << 31;
		      v ^= v >>> 8;
		      w = 4294957665L * (w & 0xffffffff) + (w >>> 32);
		      long x = u ^ (u << 21);
		      x ^= x >>> 35;
		      x ^= x << 4;
		      long ret = (x + v) ^ w;
		      return ret;
		} catch (Exception e) {
		      l.unlock();
		}
		return Long.MAX_VALUE;
	}
	
	/**
	 * Returns the next Random <i>Long</i> cast to an <i>int</i>.
	 */
	protected int next(int bits){
		return (int) (nextLong() >>> (64-bits));
	}
	
	public long seed(){return s;}
	
	public int nextInt(){return next(32);}
	public int nextInt(int n){

        if (n <= 0)
            throw new IllegalArgumentException("n must be positive");

        if ((n & -n) == n)  // i.e., n is a power of 2
            return (int)((n * (long)next(31)) >> 31);

        int bits, val;
        do {
            bits = next(31);
            val = bits % n;
        } while (bits - val + (n-1) < 0);
        return val;
	}
	
	public int nextInt(int min, int max){
		return min + nextInt(max - min + 1);
	}
}