package brosious.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * <code>FileHelper</code> is not to be instantiated. It serves simply as a class to host
 * many useful functions regarding files.
 * @author Ben
 *
 */
public abstract class FileHelper {

	/**
	 * Supply the file path and the string to be appended.
	 * @param fn
	 * @param appStr
	 */
	public void appendFile(String fn, String appStr){
		BufferedWriter out = null;
		try{
			out = new BufferedWriter(new FileWriter(fn, true));
			out.write(appStr);
				out.close();
		}catch (IOException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Provide a path and <code>true</code> to include hidden files or <code>false</code>
	 * otherwise. This function returns an array of files in the given directory's path.
	 * @param path
	 * @param hideFiles
	 * @return <code>File[]</code> files
	 */
	public File[] filesInDirectory(String path, boolean hideFiles){
		File dir = new File(path);
		File[] children = dir.listFiles();
		FilenameFilter filter = new FilenameFilter(){
			public boolean accept(File dir, String name) {
				return !name.startsWith(".");
			}
		};
		if (hideFiles){
			children = dir.listFiles(filter);
		}
		return children;
	}
	
	/**
	 * Provide a path and <code>true</code> to include hidden direcrtories or <code>false</code>
	 * otherwise. This function returns an array of directories in the given directory's path.
	 * @param path
	 * @param hideFiles
	 * @return
	 */
	public File[] listDirectories(String path, boolean hideFiles){
		File dir = new File(path);
		FileFilter fileFilter = new FileFilter(){
			public boolean accept(File file){
				return file.isDirectory();
			}
		};
		FilenameFilter filter = new FilenameFilter(){
			public boolean accept(File dir, String name) {
				return !name.startsWith(".");
			}
		};
		File[] a = dir.listFiles(fileFilter);
		File[] b = dir.listFiles(filter);
		ArrayList<File> c = new ArrayList<File>();
		
		for (int i=0; i<a.length; i++){
			for (int j=0; j<b.length; j++){
				if (a[i].equals(b[i])) c.add(a[i]);
			}
		}
		File[] arr = (File[]) c.toArray();
		return arr;
	}
}
