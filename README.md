# Fearlessagent/util #

Just some useful classes I wrote that I use in nearly every project I make. Figured I'd throw them all into a library. 

### AnsiColor ###
* Easily format strings with color for terminals.
* Helper methods to easily output colored strings.
* Supports all 18 colors for foreground and background text.

### ArrayList2D ###
* Instantiate with any type.
* Automatically handle two-dimenstional `ArrayList`s
* Requires both column and row size to instantiate, or no size at all.

### FileHelper ###
* Cannot instantiate.
* Purely for statically accessed methods.
* Easily append to files.
* Retrieve files or directory given a certain path.

### FrameRateHandler ###
* An unbiased method of keeping track of delay between screen refreshes.
* Can specify FPS or delay between frames in ms.

### HighQualityRandom ###
* "Better" random numbers at the cost of slightly slower speed
* Uses two `XORShift` generators, an `LCG` generator, and finally a `multiply with carry` generator.
* Can generate random `longs` or pre-convert to an `int`
* Uses `System.nanoTime()` for seed if none is given.